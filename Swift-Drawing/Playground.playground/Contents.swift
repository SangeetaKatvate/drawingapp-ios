//: Playground - noun: a place where people can play

import Cocoa

var str = "Hello, playground"

var scanner = NSScanner(string: str)
var matchStr:NSString?
var isFound: Bool = scanner.scanString("good", intoString:&matchStr)
if isFound{
    print("String matched")
}
else{
    print("String not found")
}
