//
//  Drawing.swift
//  Swift-Drawing
//
//  Created by Sangeeta Katvate on 25/07/16.
//  Copyright © 2016 Sangeeta Katvate. All rights reserved.
//

import UIKit

class Drawing: NSObject {
    var lineColor: UIColor!
    var lineWidth: CGFloat!
    var pointsArr:[CGPoint] = []
}
