//
//  Extension.swift
//  Swift-Drawing
//
//  Created by Sangeeta Katvate on 25/07/16.
//  Copyright © 2016 Sangeeta Katvate. All rights reserved.
//

import Foundation
import UIKit


extension UIColor{
    convenience init(hexString : String!){
        //check string contains #
        assert(hexString[hexString.startIndex] == "#" , "Expected hex string of format #RRGGBB")
       
        let scanner = NSScanner(string: hexString)
        scanner.scanLocation = 1  // skip #
        
        var rgb: UInt32 = 0
        //scan hexadecimal value and returns unsigned int 32 value
        scanner.scanHexInt(&rgb)
        
        self.init(
            red:   CGFloat((rgb & 0xFF0000) >> 16)/255.0,
            green: CGFloat((rgb &   0xFF00) >>  8)/255.0,
            blue:  CGFloat((rgb &     0xFF)      )/255.0,
            alpha: 1.0)
        
    }
}