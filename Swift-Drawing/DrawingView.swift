//
//  DrawingView.swift
//  Swift-Drawing
//
//  Created by Sangeeta Katvate on 25/07/16.
//  Copyright © 2016 Sangeeta Katvate. All rights reserved.
//

import UIKit

class DrawingView: UIView {
    
    var drawingColor : UIColor = UIColor.blackColor()
    var lineWidth: CGFloat = 2.0

    var currentDrawing: Drawing!
    var drawingArr : [Drawing] = []
    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
        let context = UIGraphicsGetCurrentContext();
        
        if drawingArr.count > 1{
            for i in 1 ..< drawingArr.count  {
                
                let drawingObj = drawingArr[i];
                
                for j in 1 ..< drawingObj.pointsArr.count {
                    CGContextMoveToPoint(context, drawingObj.pointsArr[j-1].x, drawingObj.pointsArr[j-1].y);
                    CGContextAddLineToPoint(context, drawingObj.pointsArr[j].x, drawingObj.pointsArr[j].y);
                    
                    CGContextSetStrokeColorWithColor(context, drawingObj.lineColor.CGColor);
                    CGContextSetLineWidth(context, drawingObj.lineWidth)
                    
                    CGContextStrokePath(context)
                }
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        backgroundColor = UIColor.init(hexString: "#FEFCCD")
    }
    
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if let touch = touches.first{
            let previousPoint: CGPoint = touch.locationInView(self)
            currentDrawing = Drawing()
            currentDrawing.lineColor = drawingColor
            currentDrawing.lineWidth = lineWidth;
            currentDrawing.pointsArr.append(previousPoint)
            
            drawingArr.append(currentDrawing)
        }
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if let touch = touches.first{
            let currentPoint: CGPoint = touch.locationInView(self)
            currentDrawing.pointsArr.append(currentPoint)
            self.setNeedsDisplay()
        }
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
    }
}
